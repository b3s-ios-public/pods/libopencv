#!/bin/sh

pod repo push mypod-spec libOpenCV.podspec \
					--allow-warnings \
					--use-libraries \
					--skip-tests  \
					--skip-import-validation \
					--no-private \
					--verbose
